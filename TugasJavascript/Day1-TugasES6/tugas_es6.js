// soal 1
console.log("----SOAL 1----");

const volumeBalok = (...rest) => {
  const [p, l, t] = rest;
  return p * l * t;
};

const volumeTabung = (...rest) => {
  const [r, t] = rest;
  const pi = r % 7 === 0 ? 22 / 7 : 3.14;
  return pi * r * r * t;
};

console.log(volumeBalok(4, 5, 7));
console.log(volumeTabung(7, 10));

// soal 2
console.log("----SOAL 2----");

let pesertaLomba = [
  ["Budi", "Pria", "172cm"],
  ["Susi", "Wanita", "162cm"],
  ["Lala", "Wanita", "155cm"],
  ["Agung", "Pria", "175cm"],
];

let arrOfPesertaLomba = [];

pesertaLomba.map((x) => {
  let obj = {
    nama: x[0],
    jenisKelamin: x[1],
    tinggi: x[2],
  };
  arrOfPesertaLomba.push(obj);
});

console.log(arrOfPesertaLomba);

// soal 3
console.log("----SOAL 3----");

let kalimat = "";

const tambahKata = (kata) => {
  kalimat = `${kalimat} ${kata}`;
};

tambahKata("saya");
tambahKata("adalah");
tambahKata("seorang");
tambahKata("frontend");
tambahKata("developer");

console.log(kalimat);

// soal 4

console.log("----SOAL 4----");

const newFunction = (literal = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
});

//Driver Code
newFunction("John", "Doe").fullName();

// soal 5

console.log("----SOAL 5----");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};

const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation, spell);

// soal 6

console.log("----SOAL 6----");

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

const combined = [...west, ...east];
//Driver Code
console.log(combined);

// soal 7

console.log("----SOAL 7----");

let warna = ["biru", "merah", "kuning", "hijau"];

let dataBukuTambahan = {
  penulis: "john doe",
  tahunTerbit: 2020,
};

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul: ["hitam"],
};

buku = {
  ...buku,
  warnaSampul: [...buku.warnaSampul, ...warna],
  ...dataBukuTambahan,
};

console.log(buku);

// soal 8

console.log("----SOAL 8----");

let cars = [
  { brand: "BMW", color: "red", yearOfRelease: 2010 },
  { brand: "Benz", color: "navy", yearOfRelease: 2008 },
  { brand: "Daihatsu", color: "red", yearOfRelease: 2009 },
  { brand: "Honda", color: "white", yearOfRelease: 2011 },
  { brand: "Jaguar", color: "red", yearOfRelease: 2018 },
];

let carsFilter = cars.filter(
  (x) => x.color === "red" && x.yearOfRelease > 2010
);

console.log(carsFilter);
