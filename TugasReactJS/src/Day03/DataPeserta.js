import React, { Component } from "react";
import "../App.css";

export default class DataPeserta extends Component {
  render() {
    return (
      <div className="day03__datapeserta_wrapper">
        <div className="day03__datapeserta_box">
          <h1 className="day03__datapeserta_title">Data Peserta</h1>
          <div className="day03__datapeserta_dec">
            <p>
              <strong>Nama : </strong>
              {"Andi Rustianto"}
            </p>
            <p>
              <strong>Email : </strong>
              {"yourmail@mail.com"}
            </p>
            <p>
              <strong>Sistem Operasi : </strong>
              {"Windows"}
            </p>
            <p>
              <strong>Gitlab : </strong>
              {"anditorx"}
            </p>
            <p>
              <strong>Telegram : </strong>
              {"@axaragy"}
            </p>
          </div>
        </div>
      </div>
    );
  }
}
